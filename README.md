# AWS ssh resolver

resolve server name to AWS and run ssh with it.
Can connect to a second server from the first one, usefull to connect with a bastion server

### Usage 
```
awshh (-u) [FIRST SERVER] ([NEXT SERVER])                         
 Connect to matching server (through bastion server if needed)                         
 -u      use user ubuntu
```
### Exemple 
```
awshh Bastion nginx-3
```
Will resolve both servers from Name. Next, connect to your nginx server through the bastion server
(with your unix user / key)


```
awshh -u frontend
```
Will connect to your frontend server with ubuntu user